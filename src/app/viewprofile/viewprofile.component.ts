import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../dataservice/api.service';

@Component({
  selector: 'app-viewprofile',
  templateUrl: './viewprofile.component.html',
  styleUrls: ['./viewprofile.component.scss']
})
export class ViewprofileComponent implements OnInit {

  profile_id_view:number;
  individual_response:any;

  constructor(private activatedRoute:ActivatedRoute,
    private apiService:ApiService) { }

  ngOnInit(): void {
    this.profile_id_view = this.activatedRoute.snapshot.params['id'];
    // console.log("Profile_id", this.profile_id_view)
    this.showIndividualProfile();
  }

  showIndividualProfile(){
    this.apiService.getProfileById(this.profile_id_view).subscribe(response => {
      this.individual_response = response;
      console.log("Res" + response)
    });
  }

}