export class ProfileModel {
    first_name: string;
    middle_name: string;
    last_name: string;
    designation_id:number;
    role_id:number;
}
