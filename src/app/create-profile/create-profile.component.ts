import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../dataservice/api.service';
import { ProfileModel } from '../model/profile-model';

@Component({
  selector: 'app-create-profile',
  templateUrl: './create-profile.component.html',
  styleUrls: ['./create-profile.component.scss']
})
export class CreateProfileComponent implements OnInit {
  // Profile_details_response:any;
  first_name: string;
  middle_name: string;
  last_name: string;
  designation_id:number;
  role_id:number;

  error_message: String;

  profile_model: ProfileModel;
  constructor(private router:Router,
    private apiService : ApiService,) { 
      this.profile_model = new ProfileModel();
    }

  ngOnInit(): void {
    // this.getAllProfileDetails();
  }

  createProfile() {

    this.profile_model.first_name = this.first_name;
    this.profile_model.middle_name = this.middle_name;
    this.profile_model.last_name = this.last_name;
    this.profile_model.designation_id = this.designation_id;
    this.profile_model.role_id = this.role_id;

    console.log(this.profile_model.first_name = this.first_name)

    this.apiService.createProfile(this.profile_model).subscribe(response => {
      console.log("create profile response", response);
      if (response != null) {
        this.router.navigate([''])
      } else {
        this.error_message = "No data";
      }

    }, error => {
      console.log("error", error)
    })

    // console.log("profile object", this.profile_model) use to check or debug the cose error
  }

}
