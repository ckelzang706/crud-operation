import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { PROFILE_API } from '../api.constant';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      Accept: "application/json"
    }),
  };

  handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  getAllProfileDetails(){
    return this.http
      .get<any>(
        `${PROFILE_API}` + "demo/",
        this.httpOptions
      )
      // .pipe(retry(2), catchError(this.handleError));
  }

  createProfile(profile_model){
    return this.http
      .post<any>(
        `${PROFILE_API}` + "demo/", JSON.stringify(profile_model),
        this.httpOptions
      )
      // .pipe(retry(2), catchError(this.handleError));
  }

  getProfileById(profile_id){
    return this.http
      .get<any>(
        `${PROFILE_API}` + "demo/" +profile_id,
        this.httpOptions
      )
      // .pipe(retry(2), catchError(this.handleError));
  }

  updateProfile(hotel_id, hotel_model) {
    return this.http
      .put<any>(
        `${PROFILE_API}` + "demo/" + hotel_id, JSON.stringify(hotel_model),
        this.httpOptions
      )
      // .pipe(retry(2), catchError(this.handleError));
  }
}
