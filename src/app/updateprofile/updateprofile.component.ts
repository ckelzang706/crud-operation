import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../dataservice/api.service';
import { ProfileModel } from '../model/profile-model';

@Component({
  selector: 'app-updateprofile',
  templateUrl: './updateprofile.component.html',
  styleUrls: ['./updateprofile.component.scss']
})
export class UpdateprofileComponent implements OnInit {


  first_name: string;
  middle_name: string;
  last_name: string;
  designation_id:number;
  role_id:number;

  error_message: String;

  profile_id_update:number;

  profile_model: ProfileModel;

  constructor(private activatedRouter:ActivatedRoute,
    private apiService:ApiService, 
    private router:Router) { 
      this.profile_model=  new ProfileModel()
    }

  ngOnInit(): void {
    this.profile_id_update = this.activatedRouter.snapshot.params['id'];
  }

  updateProfile(){
    this.profile_model.first_name = this.first_name;
    this.profile_model.middle_name = this.middle_name;
    this.profile_model.last_name = this.last_name;
    this.profile_model.designation_id = this.designation_id;
    this.profile_model.role_id = this.role_id;

    this.apiService.updateProfile(this.profile_id_update, this.profile_model).subscribe(response =>{
      if(response != null){
        this.router.navigate(['']);
      }else{
        return null;
      }
    })
  }

}
