import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../dataservice/api.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  Profile_details_response:any;
  constructor(private router:Router,
    private apiService : ApiService,) { }

  ngOnInit(): void {
    this.getAllProfileDetails();
  }
  getAllProfileDetails(){
    this.apiService.getAllProfileDetails().subscribe( reponse =>{
      this.Profile_details_response = reponse;
      console.log('success!!!',reponse)
    })
  }

  viewDetail(profile_id){
    this.router.navigate(['view-profile/'+profile_id])
  }

  updateProfile(profile_id){
    this.router.navigate(['update-profile/'+profile_id])
  }
}